docker-up:
	docker-compose up -d

docker-build:
	docker-compose up --build -d

docker-down:
	docker-compose down

test:
	docker-compose exec php-cli vendor/bin/phpunit

npm-dev:
	docker exec node npm run development

npm-watch:
	docker exec node npm run development -- --watch

art:
	docker exec php-cli php artisan $(command)

npm-prod:
	docker-compose exec node npm run production

composer-install:
	docker-compose exec php-cli composer install
	docker-compose exec php-cli composer run post-root-package-install
	docker-compose exec php-cli composer run post-create-project-cmd

npm-install:
	docker-compose exec node npm install



